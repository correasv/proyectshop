<?php

require_once 'lib/Controller.php';
require_once 'model/RoleModel.php';

class Login extends Controller
{

    public function __construct()
    {
        parent::__construct('Login');
//        echo "Dentro de Index<br>";
    }   
   
    public function index()
    {
        //mostrar lista de todos los registros.
        $rows = $this->model->getAll();
        $this->view->render($rows);
    }
    
    public function add()
    {
        $roleModel = new RoleModel();
        $roles = $roleModel->getAll(false);        
        $this->view->add($roles);
    }
    
    public function insert()
    {
        $row = $_POST;  
        $row['password'] = md5($row['password']);
        $this->model->insert($row);    
        header('Location: ' . Config::URL . $_SESSION['lang'] . '/user/index');
    }
    public function delete($id)
    {

    }
    
    public function edit($id, $error="")
    {

    }

    public function update()
    {

    }
    
    private function _validate($row)
    {
        $error = array();
               
        if (!preg_match("/^.{6,20}$/", $row['password'])){
            $error['password'] = 'error_password';
        }
        
        return $error;
    }
    
}
